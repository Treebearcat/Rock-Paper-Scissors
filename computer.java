import java.util.Random;

public class Computer {
	
	private int choice;
	private Random rand;
	
	public Computer(){
		this(System.currentTimeMillis());
	}
	
	public Computer(long seed){
		rand = new Random();
		rand.setSeed(seed);
	}
	
	public int getChoice(){
		return choice;
	}
	
	public void setChoice(){
		choice = rand.nextInt(Game.MAX_OPTIONS);
	}
	
	public String toString(){
		String str;
		switch(choice){
		case Game.ROCK:
			str = "rock";
			break;
		case Game.PAPER:
			str = "paper";
			break;
		case Game.SCISSORS:
			str = "scissors";
			break;
		default:
			str = "Error in toString of Computer" + choice;
		}
		return str;
	}
}