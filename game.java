import java.util.Scanner;

public class Game {
	
	public final static int MAX_OPTIONS = 3;
	public final static int ROCK = 0;
	public final static int PAPER = 1;
	public final static int SCISSORS = 2;
	
	private static Scanner scan;
	
	public static void main(String[] args) {
		Computer computer = new Computer();
		Player player = new Player();
		String playerChoice;
		boolean validInput = true;
		scan = new Scanner(System.in);
		do{
		    System.out.println("Please enter choice\nRock, Paper, Scissors or Done (to exit game):");
			playerChoice = scan.nextLine();
			playerChoice = playerChoice.toLowerCase();
			switch(playerChoice){
			case "rock":
				player.setChoice(ROCK);
				break;
			case "paper":
				player.setChoice(PAPER);
				break;
			case "scissors":
				player.setChoice(SCISSORS);
				break;
			case "done":
				System.exit(0);
			default:
				System.err.println("Input was not rock, paper or scissors");	
				validInput = false;
			}
			if(validInput){
				computer.setChoice();
				switch(computer.getChoice()){
				case ROCK:
					switch(player.getChoice()){
					case ROCK:
						System.out.println("It is a tie!");
						break;
					case PAPER:
						System.out.println("Player Wins!");
						break;
					case SCISSORS:
						System.out.println("Computer Wins!");
						break;
					default:
						System.err.println("Error in computer Rock check");
					}
				case PAPER:
					switch(player.getChoice()){
					case ROCK:
						System.out.println("Computer Wins!");
						break;
					case PAPER:
						System.out.println("It is a tie!");
						break;
					case SCISSORS:
						System.out.println("Player Wins!");
						break;
					default:
						System.err.println("Error in computer Paper check");
					}
				case SCISSORS:
					switch(player.getChoice()){
					case ROCK:
						System.out.println("Player Wins!");
						break;
					case PAPER:
						System.out.println("Computer Wins!");
						break;
					case SCISSORS:
						System.out.println("It is a tie!");
						break;
					default:
						System.err.println("Error in computer Scissors check");
					}
				default:
					System.err.println("Computer got a value that is not existing: " + computer.getChoice());
				}
			}
			System.out.println("Computer Choice: " + computer.toString() + "\nPlayer Choice: " + player.getChoice());
		}while (scan.hasNextLine());
	}
}